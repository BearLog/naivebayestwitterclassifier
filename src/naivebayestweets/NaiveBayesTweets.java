package naivebayestweets;

/**
 *
 * @author Thomas
 */
import java.io.*;
import java.util.*;
public class NaiveBayesTweets {
    Set<Set<String>> Trump = new HashSet<>();
    Set<Set<String>> Clinton = new HashSet<>();
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException{
        new NaiveBayesTweets().run();
    }
    
    public void run() throws FileNotFoundException {
        
        Scanner tweetsIn = new Scanner(new File("tweets_train.txt"));
        Scanner classIn = new Scanner(new File("labels_train.txt"));
        Set<Set<String>> donald = new HashSet<>();
        Set<Set<String>> hillary = new HashSet<>();
        while(tweetsIn.hasNext() && classIn.hasNext()){
            Scanner line = new Scanner(tweetsIn.nextLine());
            Set<String> words = new HashSet<>();
            while(line.hasNext()){
                words.add(line.next());
            }
            
            if(classIn.nextInt() == 1){
                donald.add(words);
            }else{
                hillary.add(words);
            }
        }
        Tweeter trump = new Tweeter(donald);
        Tweeter clinton = new Tweeter(hillary);
        
        testAccuracy(clinton, trump);
        promptUser(clinton, trump);
        
    }
     //runs the classifier on the test set
    public void testAccuracy(Tweeter clinton, Tweeter trump) throws FileNotFoundException{
        List<Set<String>> tweets = new ArrayList<>();
        
        Scanner testTweets = new Scanner(new File("tweets_test.txt"));
        while(testTweets.hasNext()){
            Scanner line = new Scanner(testTweets.nextLine());
            Set<String> words = new HashSet<>();
            while(line.hasNext()){
                words.add(line.next());
            }
            tweets.add(words);
        }
        
        Scanner testLabels = new Scanner(new File("labels_test.txt"));
        List<Integer> labels = new ArrayList<>();
        while(testLabels.hasNextInt()){
            labels.add(testLabels.nextInt());
        }
        
        List<Integer> classification = new ArrayList<>();
        
        for(Set<String> tweet : tweets){
            if(trump.probabilityWords(tweet) > clinton.probabilityWords(tweet)){
                classification.add(1);
            }else{
                classification.add(0);
            }
        }
        System.out.println(classification);
        
        int correct = 0;
        for (int i = 0; i < labels.size(); i++){
            if(labels.get(i) == classification.get(i)){
                correct++;
            }
        }
        System.out.println((double) correct / labels.size());
    }
    
    //classifies user input
    public void promptUser(Tweeter clinton, Tweeter trump){
        Scanner console = new Scanner(System.in);
        while(true){
            System.out.println("Give me a tweet and I'll tell you if I think it was"
                    + " written by Hilary Clinton or Donald Trump...");
            String tweet = console.nextLine();
            if (tweet.toLowerCase().equals("exit")){
                break;
            }
            Set<String> tweets = new HashSet<>();
            Scanner tweetScan = new Scanner(tweet);
            while(tweetScan.hasNext()){
                tweets.add(tweetScan.next());
            }

            if(trump.probabilityWords(tweets) > clinton.probabilityWords(tweets)){
                System.out.println("Trump");
            }else{
                System.out.println("Clinton");
            }
        }
    }
}
