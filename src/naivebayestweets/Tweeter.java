/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package naivebayestweets;

/**
 *
 * @author Thomas
 */
import java.util.*;

public class Tweeter {
    public Map<String, Integer> wordCounts;
    public int setSize;
    public Tweeter(Set<Set<String>> tweets){
        wordCounts = new HashMap<>();
        setSize = tweets.size();
        Iterator<Set<String>> tweetIt = tweets.iterator();
        while(tweetIt.hasNext()){
            Iterator<String> wordIt = tweetIt.next().iterator();
            while(wordIt.hasNext()){
                String word = wordIt.next();
                if (wordCounts.containsKey(word)){
                    wordCounts.put(word, wordCounts.get(word) + 1);
                }else{
                    wordCounts.put(word, 1);
                }
            }
            
        }
    }
    
    public double probabilityWord(String word){
        if(wordCounts.containsKey(word)){
            return (double) wordCounts.get(word) / setSize;
        }else{
            return (double) 1 / setSize;
        }
    }
    
    public double probabilityWords(Set<String> words){
        Iterator<String> it = words.iterator();
        double probability = 1;
        while(it.hasNext()){
            probability *= probabilityWord(it.next());
        }
        return probability;
    }
}
